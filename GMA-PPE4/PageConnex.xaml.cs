﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace GMA_PPE4
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class PageConnex : Page
    {
        public static string login = "";
        private const string JSONFILENAME = "datalogmdp.json";
        public PageConnex()
        {
            this.InitializeComponent();
            
        }

        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private async void Button_Click_3(object sender, RoutedEventArgs e)
        {
            
            string Log = tb_login.Text;
            string Mdp = tb_mdp.Text;

            if (Classes.Utils.verifLaCoWS(Log, Mdp) != 0)
            {
                this.Frame.Navigate(typeof(MainPage));
               login = tb_login.Text;
            }
            else
            {
                TB_result.Text = "Login ou mdp invalide !!!!!!!!!!!!!!!!! ";
            }
            /*
            bool isNetwork = NetworkInterface.GetIsNetworkAvailable();

            if (isNetwork)
            {

                if ((CreateCoup.lesClubs == null) || (CreateCoup.lesClubs.Count == 0))
                {
                    TB_result.Text = "Aucune club n'est actuellement ouverte.";
                }
                else
                {
                    using (var httpClient = new HttpClient())
                    {
                        // classe utilisée pour sérialiser des instances en JSON - ici une List de Note est spécifié pour indiquer les data à sérialiser.
                        var serializer = new DataContractJsonSerializer(typeof(ClassLibraryCT.Club));  // modif faite sur typeof() pour récupérer une note (tous le code ne concerne donc que la DERNIERE note) ATTENTION DONC LORS DE LA LECTURE DES COMMENTAIRES
                        // flux dont le contenu est en mémoire
                        var stream_post = new MemoryStream();
                        // sérialise la collection de notes et la copie dans le stream en mémoire.
                        //serializer.WriteObject(stream_post, MainPage.lesNotes);

                        serializer.WriteObject(stream_post, CreateCoup.leClub);

                        // on se cale au début du flux
                        stream_post.Position = 0;
                        // on crée un flux de lecture à partir de celui en mémoire
                        StreamReader sr = new StreamReader(stream_post);   // sr contient toutes les données des notes qui sont maintenant sérialisées

                        // on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
                        httpClient.BaseAddress = new Uri("http://127.0.0.1/projects/SCRIPT_PHP/");    // Mettre adresse locale ou localhost   <<<<<< Classe BDD au lieu de ça ? Non car Windows Phone n'accepte pas les classes en rapport avec les BDD
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        // exemple montrant un envoi de données clés valeurs sous forme de texte
                        var pairs = new List<KeyValuePair<string, string>>
                        {
                           
                            new KeyValuePair<string, string>("login", PageConnex.login),
                            new KeyValuePair<string, string>("mdp", "toto"),
                            new KeyValuePair<string, string>("data", sr.ReadToEnd())

     
                        };

                        // on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
                        var content = new FormUrlEncodedContent(pairs);

                        // formule l'appel au script distant en lui passant les données en POST
                        HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", content).Result;




                        // BIEN QUE CE SOIT 'content' QUI EST ENVOYE, LA PAGE PHP RECEVRA $_POST['login'], $_POST['mdp'], $_POST['data']

                        // DEBUG
                        //System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - content : " + response);


                        //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json")).Result;

                        string statusCode = response.StatusCode.ToString();

                        if (statusCode != "NotFound")
                        {
                            // BIEN QUE CE SOIT 'content' QUI EST ENVOYE, LA PAGE PHP RECEVRA $_POST['data']

                            response.EnsureSuccessStatusCode();
                            Task<string> responseBody = response.Content.ReadAsStringAsync();

                            // Désérialisation
                            serializer = new DataContractJsonSerializer(typeof(ClassLibraryCT.Message));
                            var stream = new MemoryStream(Encoding.UTF8.GetBytes(responseBody.Result));
                            ClassLibraryCT.Message m = (ClassLibraryCT.Message)serializer.ReadObject(stream);

                            TB_result.Text = m.Libelle;

                        }
                        else
                        {
                            MessageDialog msgbox1 = new MessageDialog("Serveur Indisponible HIHI!");
                            //Calling the Show method of MessageDialog class  
                            //which will show the MessageBox  
                            await msgbox1.ShowAsync();
                        }

                        response.EnsureSuccessStatusCode();

                        //Task<string> responseBody = response.Content.ReadAsStringAsync();

                        await deleteJsonAsync();
                    }
                }
            }
        }
            private async Task deleteJsonAsync()     //     SUPPRESSION DU CONTENU DU FICHIER LOCAL
        {
            StorageFile filed = await ApplicationData.Current.LocalFolder.GetFileAsync(JSONFILENAME);

            if (filed != null)
            {
                await filed.DeleteAsync();
            }
        }
             */
           
        }
    }
}
