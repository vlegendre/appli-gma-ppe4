﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using ClassLibraryCT;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace GMA_PPE4
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class ListeCoup : Page
    {
        public static List<Trou> lesTrou = null;
        public static List<Club> lesClubs = null;
        
        public ListeCoup()
        {
            
            this.InitializeComponent();


            /******************************************************************************************************************************
            *TEST AVEC VALEURS DIRECT EN DURE
            *
            * * ***************************************************************************************************************************
         
             List<Trou> ListTrou = new List<Trou>();
                //test d'une liste:
             ListTrou.Add(new Trou() { Numero = 1, Distance = 100, Par = 3 });
             ListTrou.Add(new Trou() { Numero = 2, Distance = 200, Par = 5 });
             ListTrou.Add(new Trou() { Numero = 3, Distance = 300, Par = 8 });
            foreach (Trou unTrou in ListTrou)
            {
                this.comboNumTrou.Items.Add(unTrou.Numero);

            }

            /* * ***************************************************************************************************************************/

          
                List<Trou> items = new List<Trou>();
                lesTrou = CreateCoup.leClub.LesTrous;
                foreach (Trou unTrou in lesTrou)
                {
                    if (unTrou.Score != 0)
                    {
                        int num = unTrou.Numero;
                        int par = unTrou.Par;
                        int dist = unTrou.Distance;
                        int score = unTrou.Score;
                        unTrou.Score = score;
                        items.Add(unTrou);

                        this.liste_des_coups.ItemsSource = items;
                    }
                }
            
        }

        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void liste_des_coups_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
