﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d'élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkId=391641

namespace GMA_PPE4
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private const string JSONFILENAME = "data.json";
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
   

            
        }

        private async Task writeJsonAsync()
        {
            // Notice that the write is ALMOST identical ... except for the serializer.

            var serializer = new DataContractJsonSerializer(typeof(ClassLibraryCT.Club));
            using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
                          JSONFILENAME,
                          CreationCollisionOption.ReplaceExisting))
            {
                serializer.WriteObject(stream, CreateCoup.leClub);
            }

            TB_result.Text = "Write succeeded";
        }

        private async Task deserializeJsonAsync()
        {
            bool fileExists = true;
            Stream myStream = null;
            StorageFile file = null;

            string content = String.Empty;

            try
            {
                file = await ApplicationData.Current.LocalFolder.GetFileAsync(JSONFILENAME);
                myStream = await file.OpenStreamForReadAsync();
                myStream.Dispose();

               // ClassLibraryCT.Club mes_notes;

                var jsonSerializer = new DataContractJsonSerializer(typeof(ClassLibraryCT.Club));

                myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(JSONFILENAME);

                //mes_notes = (ClassLibraryCT.Club)jsonSerializer.ReadObject(myStream);

 
            }
            catch (FileNotFoundException)
            {
                // If the file dosn't exits it throws an exception, make fileExists false in this case 
                fileExists = false;
                content = "Fichier de sauvegarde inexistant.";
            }
            finally
            {
                if (myStream != null)
                {
                    myStream.Dispose();
                }
            }

            TB_result.Text = content;
        }
        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d’événement décrivant la manière dont l’utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // TODO: préparer la page pour affichage ici.

            // TODO: si votre application comporte plusieurs pages, assurez-vous que vous
            // gérez le bouton Retour physique en vous inscrivant à l’événement
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed.
            // Si vous utilisez le NavigationHelper fourni par certains modèles,
            // cet événement est géré automatiquement.
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(PageConnex));
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(CreateCoup));
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {

            if (CreateCoup.leClub != null)
            { 
                this.Frame.Navigate(typeof(ListeCoup)); 
            }
            else
            {
                
                TB_result.Text = " Il n'y a aucun parcours en cours ";
            }
        }

        private async void Button_Click_3(object sender, RoutedEventArgs e)
        {
            bool isNetwork = NetworkInterface.GetIsNetworkAvailable();

            if (isNetwork)
            {

                if ((CreateCoup.lesClubs == null) || (CreateCoup.lesClubs.Count == 0) )
                {
                    TB_result.Text = "Aucune club n'est actuellement ouverte.";
                }
                else
                {
                    using (var httpClient = new HttpClient())
                    {
                        // classe utilisée pour sérialiser des instances en JSON - ici une List de Note est spécifié pour indiquer les data à sérialiser.
                        var serializer = new DataContractJsonSerializer(typeof(ClassLibraryCT.Club));  // modif faite sur typeof() pour récupérer une note (tous le code ne concerne donc que la DERNIERE note) ATTENTION DONC LORS DE LA LECTURE DES COMMENTAIRES
                        // flux dont le contenu est en mémoire
                        var stream_post = new MemoryStream();
                        // sérialise la collection de notes et la copie dans le stream en mémoire.
                        //serializer.WriteObject(stream_post, MainPage.lesNotes);

                        serializer.WriteObject(stream_post, CreateCoup.leClub);

                        // on se cale au début du flux
                        stream_post.Position = 0;
                        // on crée un flux de lecture à partir de celui en mémoire
                        StreamReader sr = new StreamReader(stream_post);   // sr contient toutes les données des notes qui sont maintenant sérialisées

                        // on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
                        httpClient.BaseAddress = new Uri("http://127.0.0.1/projects/SCRIPT_PHP/");    // Mettre adresse locale ou localhost   <<<<<< Classe BDD au lieu de ça ? Non car Windows Phone n'accepte pas les classes en rapport avec les BDD
                        httpClient.DefaultRequestHeaders.Accept.Clear();
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        // exemple montrant un envoi de données clés valeurs sous forme de texte
                        var pairs = new List<KeyValuePair<string, string>>
                        {
                           
                            new KeyValuePair<string, string>("login", PageConnex.login),
                            new KeyValuePair<string, string>("mdp", "toto"),
                            new KeyValuePair<string, string>("data", sr.ReadToEnd())

     
                        };

                        // on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
                        var content = new FormUrlEncodedContent(pairs);

                        // formule l'appel au script distant en lui passant les données en POST
                        HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", content).Result;




                        // BIEN QUE CE SOIT 'content' QUI EST ENVOYE, LA PAGE PHP RECEVRA $_POST['login'], $_POST['mdp'], $_POST['data']

                        // DEBUG
                        //System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - content : " + response);


                        //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json")).Result;

                        string statusCode = response.StatusCode.ToString();

                        if (statusCode != "NotFound")
                        {
                            // BIEN QUE CE SOIT 'content' QUI EST ENVOYE, LA PAGE PHP RECEVRA $_POST['data']

                            response.EnsureSuccessStatusCode();
                            Task<string> responseBody = response.Content.ReadAsStringAsync();

                            // Désérialisation
                            serializer = new DataContractJsonSerializer(typeof(ClassLibraryCT.Message));
                            var stream = new MemoryStream(Encoding.UTF8.GetBytes(responseBody.Result));
                            ClassLibraryCT.Message m = (ClassLibraryCT.Message)serializer.ReadObject(stream);

                            TB_result.Text = m.Libelle;

                        }
                        else
                        {
                            MessageDialog msgbox1 = new MessageDialog("Serveur Indisponible HIHI!");
                            //Calling the Show method of MessageDialog class  
                            //which will show the MessageBox  
                            await msgbox1.ShowAsync();
                        }

                        response.EnsureSuccessStatusCode();

                        //Task<string> responseBody = response.Content.ReadAsStringAsync();

                        await deleteJsonAsync();
                    }
                }
            }
        }

        private async Task deleteJsonAsync()     //     SUPPRESSION DU CONTENU DU FICHIER LOCAL
        {
            StorageFile filed = await ApplicationData.Current.LocalFolder.GetFileAsync(JSONFILENAME);

            if (filed != null)
            {
                await filed.DeleteAsync();
            }
        }

        private async void Button_Click_6(object sender, RoutedEventArgs e)
        {
            await writeJsonAsync();
           // await readJsonAsync();
        }
    }
}
