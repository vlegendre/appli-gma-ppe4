﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Web.Script.Serialization;
using ClassLibraryDAO;
using ClassLibraryCT;

namespace WebApplicationTeste
{
    /// <summary>
    /// Description résumée de WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Pour autoriser l'appel de ce service Web depuis un script à l'aide d'ASP.NET AJAX, supprimez les marques de commentaire de la ligne suivante. 
    [System.Web.Script.Services.ScriptService]
    public class WebService1 : System.Web.Services.WebService
    {


        [WebMethod]
        public string GetTrousfromClub(string codeClub)
        {
            string retour = "";

            DAOClub daoClub = new DAOClub();
            Club c = daoClub.getAClubById(codeClub);
            DAOTrou dao = new DAOTrou();
            List<Trou> lesTrous = dao.getAllTrousFromClub(c);

            JavaScriptSerializer json_trous = new JavaScriptSerializer();
            retour = json_trous.Serialize(lesTrous);

            return retour;
        }

        [WebMethod]
        public string getAClubByCodePostale(string cp)
        {
            string retour = "";

            DAOClub daoClub = new DAOClub();
            List<Club> lesClubs = daoClub.getAClubByCodePostale(cp);
            JavaScriptSerializer json_clubs = new JavaScriptSerializer();
            retour = json_clubs.Serialize(lesClubs);

            return retour;
        }
        [WebMethod]
        public string getAClubByVille(string ville)
        {
            string retour = "";

            DAOClub daoClub = new DAOClub();
            List<Club> lesClubs = daoClub.getAClubByVille(ville);
            JavaScriptSerializer json_clubs = new JavaScriptSerializer();
            retour = json_clubs.Serialize(lesClubs);

            return retour;
        }

        [WebMethod]
        public string getAClubByNom(string nom)
        {
            string retour = "";

            DAOClub daoClub = new DAOClub();
            List<Club> lesClubs = daoClub.getAClubByNom(nom);
            JavaScriptSerializer json_clubs = new JavaScriptSerializer();
            retour = json_clubs.Serialize(lesClubs);

            return retour;
        }


        [WebMethod]
        public string GetAllClubUniquesearch(string info)
        {
            string retour = "";

            DAOClub daoClub = new DAOClub();
            List<Club> c = daoClub.getListeClub(info);
            JavaScriptSerializer json_trous = new JavaScriptSerializer();
            retour = json_trous.Serialize(c);

            return retour;
        }


        [WebMethod]
        public string VerifCo(string login, string mdp)
        {
            string retour = "";

            DAOConnexion daoCo = new DAOConnexion();
            int resultat = daoCo.verfico(login, mdp);
            JavaScriptSerializer json_co = new JavaScriptSerializer();
            retour = json_co.Serialize(resultat);

            return retour;
        }
    }
}
