﻿using ClassLibraryCT;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace GMA_PPE4
{
       
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class CreateCoup : Page
    {
        //public static List<Trou> lesTrou = null;
        public static List<Club> lesClubs = null;
        public static Club leClub = null;

        private const string JSONFILENAME = "data.json";

        public CreateCoup()
        {
            this.InitializeComponent();



        }

        


        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void Button_ClickRecherche(object sender, RoutedEventArgs e)
        {
            string infotext = tb_uniquesearch.Text;
            comboClub.Items.Clear();
            comboNumTrou.Items.Clear();
            
            lesClubs = Classes.Utils.recupererLesClubstotal(infotext);
            foreach (Club unClub in lesClubs)
            {
                string NomC = unClub.Nom;
                this.comboClub.Items.Add(NomC);
            }
                
        }
    
           



                                            /******************************************************************************************************************************
                                            *TEST AVEC VALEURS DIRECT DANS LA COMBOBOX AVEC LE BOUTON "RECHERCHE"
                                            *
                                            * * ***************************************************************************************************************************
         
				                                 * * List<Club> ListeClub = new List<Club>();
			                                   //test d'une liste:
			                                   ListeClub.Add(new Club() { Code = "C0002", Nom = "SIO1 golfClub", Rue = "rue de la prairie", Cp = "55000", Ville = "BAR LEDUC", Gps_lat = 48.758224, Gps_lon = 5.115337, Tel = "0329845151", Email = "siogolfclubsio2@sio.fr", Licencie = 12 });
			                                   ListeClub.Add(new Club() { Code = "C0003", Nom = "SIO2 golfClub", Rue = "rue de la prairie", Cp = "55000", Ville = "BAR LEDUC", Gps_lat = 48.758224, Gps_lon = 5.115337, Tel = "0329845151", Email = "siogolfclubsio1@sio.fr", Licencie = 33 });
			                                   //this.Combo_Club.ItemsSource = ListeClub;
			                                   foreach (Club unClub in ListeClub)
			                                   {
				                                   this.comboClub.Items.Add(unClub.Nom);

			                                   }
			                                 * ****************************************************************************************************************************/
        

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();     
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {


        }

        private void Button_ClickValider(object sender, RoutedEventArgs e)
        {
            string content = comboClub.SelectedItem.ToString();
            //System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - Club récupéré : " + content);
            comboNumTrou.Items.Clear();

            foreach (Club unClub in lesClubs)
            {
                if (content == unClub.Nom)
                {
                    CreateCoup.leClub = unClub;
                }
            }

            CreateCoup.leClub.LesTrous = Classes.Utils.recupererLesTrouClubWS(CreateCoup.leClub.Code);
            foreach (Trou unTrou in CreateCoup.leClub.LesTrous)
            {
                string NumTC = unTrou.Numero.ToString();
                this.comboNumTrou.Items.Add(NumTC);
            }  
 

                                                    /******************************************************************************************************************************
                                                    *TEST AVEC VALEURS DIRECT EN DURE
                                                    *
                                                    * * ***************************************************************************************************************************
         
                                                     List<Trou> ListTrou = new List<Trou>();
                                                        //test d'une liste:
                                                     ListTrou.Add(new Trou() { Numero = 1, Distance = 100, Par = 3 });
                                                     ListTrou.Add(new Trou() { Numero = 2, Distance = 200, Par = 5 });
                                                     ListTrou.Add(new Trou() { Numero = 3, Distance = 300, Par = 8 });
                                                    foreach (Trou unTrou in ListTrou)
                                                    {
                                                        this.comboNumTrou.Items.Add(unTrou.Numero);

                                                    }

                                                    */ 
        }
        private void Button_valider(object sender, RoutedEventArgs e)
        {
            string infotext = tb_nbcoup.Text;
            int infoint = Convert.ToInt16(infotext);
            if (infotext!="")
               {
                string idTrou=this.comboNumTrou.SelectedItem.ToString();
                foreach (Trou unTrou in CreateCoup.leClub.LesTrous)
                    {
                        string idTrourecherche = unTrou.Numero.ToString();
                        if (idTrourecherche == idTrou)
                        {
                            unTrou.Score = infoint;
                            TB_infovalider.Text = "Score validé";
                        }
                        
                    }
               }
        }

        private void Button_ClickClear(object sender, RoutedEventArgs e)
        {
            comboClub.Items.Clear();
            tb_uniquesearch.Text = "";
            comboNumTrou.Items.Clear();
          // permet de bloquer le champs texte              ::::  tb_uniquesearch.IsEnabled= false;

        }

        private async void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            await deserializeJsonAsync();

            if (CreateCoup.leClub != null)
            {
                comboClub.Items.Add(CreateCoup.leClub.Nom);
                comboClub.SelectedItem = CreateCoup.leClub.Nom;
                foreach (Trou unTrou in CreateCoup.leClub.LesTrous)
                {
                    if (unTrou.Score == 0)
                    {
                        comboNumTrou.Items.Add(unTrou.Numero);
                        comboNumTrou.SelectedItem = unTrou.Numero;
                    }
                }

                if (comboNumTrou.Items.Count() == 0)
                {
                    TB_infovalider.Text = "Votre parcours est fini, veuillez selectionner un autre club!";
                    comboClub.Items.Clear();
                    comboNumTrou.Items.Clear();
                }
            }

        }

        private async Task deserializeJsonAsync()
        {
            bool fileExists = true;
            Stream myStream = null;
            StorageFile file = null;

            string content = String.Empty;

            CreateCoup.leClub = null;

            try
            {
                file = await ApplicationData.Current.LocalFolder.GetFileAsync(JSONFILENAME);
                myStream = await file.OpenStreamForReadAsync();
                myStream.Dispose();

                Club leClub = null;

                var jsonSerializer = new DataContractJsonSerializer(typeof(Club));

                myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(JSONFILENAME);

                leClub = (Club)jsonSerializer.ReadObject(myStream);

                CreateCoup.leClub = leClub;
            }
            catch (FileNotFoundException)
            {
                // If the file dosn't exits it throws an exception, make fileExists false in this case 
                fileExists = false;
                content = "Fichier de sauvegarde inexistant.";
            }
            finally
            {
                if (myStream != null)
                {
                    myStream.Dispose();
                }
            }
        }

    }
}
