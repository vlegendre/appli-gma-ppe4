﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryCT
{
    public  class Membre
    {

        private int idmembre;
        public int idMembre
        {
            get { return idmembre; }
            set { idmembre = value; }
        }  
        private string nom;
        public string Nom
        {
            get { return nom; }
            set { nom = value; }
        }

        private string prenom;

        public string Prenom
        {
            get { return prenom; }
            set { prenom = value; }
        }
        private string login;

        public string Login
        {
            get { return login; }
            set { login = value; }
        }

        private string mdp;

        public string Mdp
        {
            get { return mdp; }
            set { mdp = value; }
        }
        



         public Membre()
        {

        }
        /*
         * Un constructeur qui valorise tous les attributs
         * */
        public Membre(int id, string n, string p, string l, string m)
        {
            this.idmembre = id;
            this.nom = n;
            this.prenom = p;
            this.login = l;
            this.mdp = m;
    

        }

        public string ToString()
        {
            return "Informations membres"+this.idmembre + " " + this.nom + " " + this.prenom + " "+ this.login + " "+ this.mdp + "";
        }
    }
}

    
