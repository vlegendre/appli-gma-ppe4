﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Globalization;
using ClassLibraryCT;


namespace ClassLibraryDAO
{
    public class DAOClub
    {
        private MySqlConnection conn;
        public DAOClub()
        {
            string myConnectionString;
            //myConnectionString = "server=193.252.48.172;port=20433;uid=golfede;pwd=golfede;database=golfbdd;";
            myConnectionString = "server=172.18.207.102;port=3306;uid=golfede;pwd=golfede;database=golfbdd;";
            conn = new MySql.Data.MySqlClient.MySqlConnection();
            conn.ConnectionString = myConnectionString;
            conn.Open();
        }

        public List<Club> getAllClubs()
        {
            List<Club> lesClubsRecuperes = new List<Club>();

            string requete = "select * from club";

            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                Club c = new Club();
                c.Code = rdr[0].ToString();
                c.Nom = rdr[1].ToString();
                c.Rue = rdr[2].ToString();
                c.Cp = rdr[3].ToString();
                c.Ville = rdr[4].ToString();
                c.Gps_lat = Convert.ToDouble(rdr[5].ToString());
                c.Gps_lon = Convert.ToDouble(rdr[6].ToString());
                c.Tel = rdr[7].ToString();
                c.Email = rdr[8].ToString();
                c.Licencie = Convert.ToInt16(rdr[9].ToString());

                DAOTrou daot = new DAOTrou();
                c.setLesTrous(daot.getAllTrousFromClub(c));

                lesClubsRecuperes.Add(c);
            }
            rdr.Close();
            return lesClubsRecuperes;
        }

        public Club getAClubById(string id)
        {
            Club c = null;
            string requete = "select * from club where code = '" + id + "'";

            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read())
            {
                c = new Club();
                c.Code = rdr[0].ToString();
                c.Nom = rdr[1].ToString();
                c.Rue = rdr[2].ToString();
                c.Cp = rdr[3].ToString();
                c.Ville = rdr[4].ToString();
                c.Gps_lat = Convert.ToDouble(rdr[5].ToString());
                c.Gps_lon = Convert.ToDouble(rdr[6].ToString());
                c.Tel = rdr[7].ToString();
                c.Email = rdr[8].ToString();
                c.Licencie = Convert.ToInt16(rdr[9].ToString());


            }
            rdr.Close();
            return c;
        }
        public List<Club> getAClubByCodePostale(string codePostale)
        {
            List<Club> lesClubsRecuperes = new List<Club>();

            string requete = "select * from club where cp = '" + codePostale + "'";

            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                Club c = new Club();
                c.Code = rdr[0].ToString();
                c.Nom = rdr[1].ToString();
                c.Rue = rdr[2].ToString();
                c.Cp = rdr[3].ToString();
                c.Ville = rdr[4].ToString();
                c.Gps_lat = Convert.ToDouble(rdr[5].ToString());
                c.Gps_lon = Convert.ToDouble(rdr[6].ToString());
                c.Tel = rdr[7].ToString();
                c.Email = rdr[8].ToString();
                c.Licencie = Convert.ToInt16(rdr[9].ToString());

                DAOTrou daot = new DAOTrou();
                c.setLesTrous(daot.getAllTrousFromClub(c));

                lesClubsRecuperes.Add(c);
            }
            rdr.Close();
            return lesClubsRecuperes;
        }
        public List<Club> getAClubByNom(string nom)
        {
            List<Club> lesClubsRecuperes = new List<Club>();

            string requete = "select * from club where nom = '" + nom + "'";

            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                Club c = new Club();
                c.Code = rdr[0].ToString();
                c.Nom = rdr[1].ToString();
                c.Rue = rdr[2].ToString();
                c.Cp = rdr[3].ToString();
                c.Ville = rdr[4].ToString();
                c.Gps_lat = Convert.ToDouble(rdr[5].ToString());
                c.Gps_lon = Convert.ToDouble(rdr[6].ToString());
                c.Tel = rdr[7].ToString();
                c.Email = rdr[8].ToString();
                c.Licencie = Convert.ToInt16(rdr[9].ToString());

                DAOTrou daot = new DAOTrou();
                c.setLesTrous(daot.getAllTrousFromClub(c));

                lesClubsRecuperes.Add(c);
            }
            rdr.Close();
            return lesClubsRecuperes;
        }
        public List<Club> getAClubByVille(string ville)
        {
            List<Club> lesClubsRecuperes = new List<Club>();

            string requete = "select * from club where ville = '" + ville + "'";

            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                Club c = new Club();
                c.Code = rdr[0].ToString();
                c.Nom = rdr[1].ToString();
                c.Rue = rdr[2].ToString();
                c.Cp = rdr[3].ToString();
                c.Ville = rdr[4].ToString();
                c.Gps_lat = Convert.ToDouble(rdr[5].ToString());
                c.Gps_lon = Convert.ToDouble(rdr[6].ToString());
                c.Tel = rdr[7].ToString();
                c.Email = rdr[8].ToString();
                c.Licencie = Convert.ToInt16(rdr[9].ToString());

                DAOTrou daot = new DAOTrou();
                c.setLesTrous(daot.getAllTrousFromClub(c));

                lesClubsRecuperes.Add(c);
            }
            rdr.Close();
            return lesClubsRecuperes;
        }



        public List<Club> getListeClub(string info)
        {
            List<Club> lesClubsRecuperes = new List<Club>();

            string requete = "select * from club where nom LIKE '%" + info + "%'  or ville LIKE '%" + info + "%' or cp LIKE '%" + info + "%' ";

            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                Club c = new Club();
                c.Code = rdr[0].ToString();
                c.Nom = rdr[1].ToString();
                c.Rue = rdr[2].ToString();
                c.Cp = rdr[3].ToString();
                c.Ville = rdr[4].ToString();
                c.Gps_lat = Convert.ToDouble(rdr[5].ToString());
                c.Gps_lon = Convert.ToDouble(rdr[6].ToString());
                c.Tel = rdr[7].ToString();
                c.Email = rdr[8].ToString();
                c.Licencie = Convert.ToInt16(rdr[9].ToString());

                DAOTrou daot = new DAOTrou();
                c.setLesTrous(daot.getAllTrousFromClub(c));

                lesClubsRecuperes.Add(c);
            }
            rdr.Close();
            return lesClubsRecuperes;
        }
    }
}
