﻿using ClassLibraryCT;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GMA_PPE4.Classes
{
    class Utils
    {
        public static List<Club> recupererLesClubsCPWS(string cp)
        {
            /******************************************************************************************************************************
            *ENVOIE DES INFORMATIONS DANS LA COMBOBOX AVEC LE BOUTON "RECHERCHE" VIA LE WEB SERVICE
            *
            * * ****************************************************************************************************************************/

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new
            MediaTypeWithQualityHeaderValue("text/xml"));
            httpClient.DefaultRequestHeaders.Add("SOAPAction",
            "http://tempuri.org/getAClubByCodePostale");
            var soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><getAClubByCodePostale xmlns=\"http://tempuri.org/\"><cp>"+cp+"</cp></getAClubByCodePostale></soap:Body></soap:Envelope>";
            var response = httpClient.PostAsync("http://localhost:2263/WS_GMA_VLSTasmx.asmx", new StringContent(soapXml, Encoding.UTF8, "text/xml")).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            XDocument responseXML = XDocument.Parse(content);
            string retour = responseXML.Root.Value;

            /////////////////probleme survenu car appel à un "typeof list" alors que la méthode geteClubByCodePostale renvoie 1club bug de l'applis
            ///////////////////////////////////**** PROBLEME VERIFIER LE 23/03/2015 , fonctionne

            var jsonSerializer = new DataContractJsonSerializer(typeof(List<Club>));
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(retour));
            List<Club> maliste = (List<Club>)jsonSerializer.ReadObject(stream);

            return maliste;
        }

        public static List<Trou> recupererLesTrouClubWS(string id)
        {
            /******************************************************************************************************************************
            *ENVOIE DES INFORMATIONS DANS LA COMBOBOX AVEC LE BOUTON "RECHERCHE" VIA LE WEB SERVICE
            *
            * * ****************************************************************************************************************************/

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new
            MediaTypeWithQualityHeaderValue("text/xml"));
            httpClient.DefaultRequestHeaders.Add("SOAPAction",
            "http://tempuri.org/GetTrousfromClub");
            var soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><GetTrousfromClub xmlns=\"http://tempuri.org/\"><codeClub>"+id+"</codeClub></GetTrousfromClub></soap:Body></soap:Envelope>";
            var response = httpClient.PostAsync("http://localhost:2263/WS_GMA_VLST.asmx", new StringContent(soapXml, Encoding.UTF8, "text/xml")).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            XDocument responseXML = XDocument.Parse(content);
            string retour = responseXML.Root.Value;

            /////////////////probleme survenu car appel à un "typeof list" alors que la méthode geteClubByCodePostale renvoie 1club bug de l'applis
            ///////////////////////////////////**** PROBLEME VERIFIER LE 23/03/2015 , fonctionne

            var jsonSerializer = new DataContractJsonSerializer(typeof(List<Trou>));
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(retour));
            List<Trou> maliste = (List<Trou>)jsonSerializer.ReadObject(stream);

            return maliste;
        }



        public static List<Club> recupererLesClubstotal(string info)
        {
            /******************************************************************************************************************************
            *ENVOIE DES INFORMATIONS DANS LA COMBOBOX AVEC LE BOUTON "RECHERCHE" VIA LE WEB SERVICE
            *
            * * ****************************************************************************************************************************/

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new
            MediaTypeWithQualityHeaderValue("text/xml"));
            httpClient.DefaultRequestHeaders.Add("SOAPAction",
            "http://tempuri.org/GetAllClubUniquesearch");
            var soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><GetAllClubUniquesearch xmlns=\"http://tempuri.org/\"><info>" + info + "</info></GetAllClubUniquesearch></soap:Body></soap:Envelope>";
            var response = httpClient.PostAsync("http://localhost:2263/WS_GMA_VLST.asmx", new StringContent(soapXml, Encoding.UTF8, "text/xml")).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            XDocument responseXML = XDocument.Parse(content);
            string retour = responseXML.Root.Value;

            /////////////////probleme survenu car appel à un "typeof list" alors que la méthode geteClubByCodePostale renvoie 1club bug de l'applis
            ///////////////////////////////////**** PROBLEME VERIFIER LE 23/03/2015 , fonctionne

            var jsonSerializer = new DataContractJsonSerializer(typeof(List<Club>));
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(retour));
            List<Club> maliste = (List<Club>)jsonSerializer.ReadObject(stream);

            return maliste;
        }
        public static int verifLaCoWS(string login, string mdp)
        {
            /******************************************************************************************************************************
            *ENVOIE DES INFORMATIONS DANS LA COMBOBOX AVEC LE BOUTON "RECHERCHE" VIA LE WEB SERVICE
            *
            * * ****************************************************************************************************************************/

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new
            MediaTypeWithQualityHeaderValue("text/xml"));
            httpClient.DefaultRequestHeaders.Add("SOAPAction",
            "http://tempuri.org/VerifCo");
            var soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><VerifCo xmlns=\"http://tempuri.org/\"><login>" + login + "</login><mdp>" + mdp + "</mdp></VerifCo></soap:Body></soap:Envelope>";
            var response = httpClient.PostAsync("http://localhost:2263/WS_GMA_VLST.asmx", new StringContent(soapXml, Encoding.UTF8, "text/xml")).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            XDocument responseXML = XDocument.Parse(content);
            string retour = responseXML.Root.Value;

            //var response = httpClient.PostAsync("http://172.18.207.204:8084/WS_GMA_VLST.asmx", new StringContent(soapXml, Encoding.UTF8, "text/xml")).Result;
            /////////////////probleme survenu car appel à un "typeof list" alors que la méthode geteClubByCodePostale renvoie 1club bug de l'applis
            ///////////////////////////////////**** PROBLEME VERIFIER LE 23/03/2015 , fonctionne

            var jsonSerializer = new DataContractJsonSerializer(typeof(int));
            var stream = new MemoryStream(Encoding.UTF8.GetBytes(retour));
            int resulat = (int)jsonSerializer.ReadObject(stream);

            return resulat;
        }
  
    }
}
